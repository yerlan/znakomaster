$(document).ready(function() {
	var ts = new Date(2016, 3, 19,23,0,0,0),
	newYear = false;

	jQuery('.countdown').countdown({
		timestamp	: ts			
	});	
	jQuery('.countdown_two').countdown({
		timestamp	: ts			
	});	
});

// // 20%
// jQuery(document).ready(function($) {
// 	var prices = $(".product-price");
// 	// console.info(prices);
// 	var old_prices = $(".old-price");
// 	// console.info(old_prices);
// 	old_prices.each(function(index, el) {
// 		var new_price = prices[index].innerText.replace('тг.', '').replace(' ', '');
// 		new_price = parseInt(new_price)
// 		var raznica = parseInt((new_price*20)/100);
// 		var old_price = new_price + raznica;
// 		old_price = parseInt(old_price)
// 		$(this).append(old_price + " тг.");

// 	});
// });


// request call modal window
$(document).ready(function() {
	$("#button-request-call").click(function(event) {
		$("#requestCallWindow").show();
    $("#requestCallWindow").reveal({
      animation: 'fade',                   //fade, fadeAndPop, none
      animationspeed: 200,                       //how fast animtions are
      closeonbackgroundclick: true,              //if you click background will modal close?
    });
	});
});


$(document).ready(function() {
	$("#call-to-price").click(function(event) {
		$("#requestCallWindow").show();
    $("#requestCallWindow").reveal({
      animation: 'fade',                   //fade, fadeAndPop, none
      animationspeed: 200,                       //how fast animtions are
      closeonbackgroundclick: true,              //if you click background will modal close?
    });
	});
});

// request call modal window (dop)
$(document).ready(function() {
	$("#button-request-call-dop").click(function(event) {
		$("#requestCallWindow").show();
    $("#requestCallWindow").reveal({
      animation: 'fade',                   //fade, fadeAndPop, none
      animationspeed: 200,                       //how fast animtions are
      closeonbackgroundclick: true,              //if you click background will modal close?
    });
	});
});



// price calc modal window
$(document).ready(function() {
	$("#button-price-calc").click(function(event) {
		$("#priceCalcWindow").show();
    $("#priceCalcWindow").reveal({
      animation: 'fade',                   //fade, fadeAndPop, none
      animationspeed: 200,                       //how fast animtions are
      closeonbackgroundclick: true,              //if you click background will modal close?
    });
	});
});

// определение страны и установка валюты
$.get("http://ipinfo.io", function(response) {
		if(response && response != ""){

			// если Казахстан
			if (response.country == "KZ") {
				
			}
			// если другая страна - преобразование цен
			else{

				// преобразование тенге в рубли(значения)
				var currency_value = $(".currency_value");
				currency_value.each(function(index, el) {
					var ruble_value = parseInt(el.innerText) / 5;
					$(this).html(ruble_value);
				});
				// преобразование тенге в рубли(лейблы)
				var currency_label = $(".currency_label");
				currency_label.each(function(index, el) {
					$(this).html("руб.");
				});
				
			}

		}
}, "jsonp");

// определение страны и установка лейбла страны
$.get("http://ipinfo.io", function(response) {
		if(response && response != ""){

			// если Казахстан
			if (response.country == "KZ") {

			}
			// если другая страна
			else{
				var cities = $(".city-label");
				cities.each(function(index, el) {
					$(this).html("России");
				});
				$('.address-info').each(function(index, el) {
					$(this).html("Адрес: г.Москва, улица Удальцова, 4, 26");
				});
			}

		}
}, "jsonp");